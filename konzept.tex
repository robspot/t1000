%!TEX root = t1000.tex
\section{Infrastruktur Konzept}
\label{sec:konzept}
\subsection{Anforderungen}
\label{sec:anforderungen}
Wie in der Zielsetzung bereits erwähnt, ist es das Ziel dieser Arbeit eine neue Messaging Infrastruktur basierend auf RabbitMQ einzurichten. Neben der Grundfunktionalität von RabbitMQ muss auch die Anbindung an bereits bestehende Systeme geschaffen werden. Dieses Kapitel behandelt die Anforderungen an das neue Messaging System.\\
\\
Da diese Arbeit sich nicht mit der Auswahl eines Messaging-Systems beschäftigt, wird das Verwenden von RabbitMQ als gegeben angesehen. Die Anforderungen basieren zum einen auf den Problemen die sich durch die aktuelle Umsetzung einer \ac{mom} bei der \ac{sit} ergeben haben. (siehe \nameref{sec:komplikationen}) Zum anderen auf Anfragen von anderen Fachbereichen, nach einem RabbitMQ Messaging System.

\subsubsection{Grundlegende Architektur}
Die Applikation RabbitMQ wird keine bestehenden Systeme bei der \ac{sit} ersetzen, sondern parallel zu diesen betrieben werden. Man kann RabbitMQ auf zwei Arten verwenden. Zum einen wie \autoref{fig:va_rabbit} darstellt als \ac{mom} in einer \ac{va}.
\begin{figure}
\begin{center}
\includegraphics[scale=0.7]{Bilder/va_rabbit.png}\\
\caption{RabbitMQ in einer verteilten Anwendung (Eigene Darstellung)}
\label{fig:va_rabbit}
\end{center}
\end{figure}
Wie die \autoref{fig:integration_rabbit} zeigt, wird RabbitMQ auch als Integrations Werkzeug zwischen verschiedenen Anwendungen verwendet werden. Dies wird durch die Broker-Unabhängigkeit von \ac{amqp} unterstützt. Diese beiden Arten der Verwendung müssen bei der Einführung bei der \ac{sit} gewährleistet werden.
\begin{figure}
\begin{center}
\includegraphics[scale=0.7]{Bilder/integration_rabbit.png}\\
\caption{RabbitMQ als Integrations-Werkzeug zwischen verschiedenen Anwendungen (Eigene Darstellung)}
\label{fig:integration_rabbit}
\end{center}
\end{figure}
Desweitern soll RabbitMQ in einem Container-Umfeld betrieben werden. Dies ist die Grundlage für eine gute Skalierbarkeit, eine einfache Entwicklung, sowie ein schnelles Deployment. Da sich die Kunden einzelne RabbitMQ-Instanzen selbst beantragen können, wird es vermutlich eine große Anzahl an einzelnen Instanzen geben. Diese müssen, wie in \autoref{fig:rabbitmq_aufbau} dargestellt, voneinander separiert werden. Da die \ac{sit} bereits Openshift betreibt, wird RabbitMQ im Openshift-Umfeld realisiert werden.
\begin{figure}
\begin{center}
\includegraphics[scale=0.7]{Bilder/rabbitmq_aufbau.png}\\
\caption{Beispiel Aufbau einer RabbitMQ-Umgebung (Eigene Darstellung)}
\label{fig:rabbitmq_aufbau}
\end{center}
\end{figure}

\subsubsection{Messaging-Funktionalität}
Das neue Messaging System soll standardmäßig das Protokoll AMQP unterstützen, da dies aufgrund seiner ausgefeilten Routing-Funktionalität sowie Plattform-Unabhängigkeit sehr vielseitig einsetzbar ist. Auch \ac{mqtt} soll standardmäßig unterstützt werden, sodass es jederzeit durch die Aktivierung des \ac{mqtt} Plugins verwendet werden kann. Eine standardmäßige Aktivierung direkt nach dem Deployment ist nicht vorgesehen, da dadurch mehrere Exchange Objekte entstehen. Diese Exchange-Objekte werden nur bei einer Verwendung von \ac{mqtt} benötigt und sind deshalb im exklusiven \ac{amqp} Betrieb nicht relevant. Des Weiteren soll es auch möglich sein die anderen Messaging Protokolle, die von RabbitMQ bereitgestellt werden, bei Bedarf nutzen zu können. Dazu gehört das \ac{stomp} sowie die auf Websockets basierenden Varianten von \ac{mqtt} und \ac{stomp}.

\subsubsection{Verfügbarkeit und Zuverlässigkeit}
Da manche Anwendungen RabbitMQ als zentrale Schnittstelle verwenden, würde ein Ausfall von RabbitMQ eine korrekte Ausführung verhindern. Um Auswirkungen durch einen Ausfall auf die Systeme des Kunden zu vermeiden, ist eine hohe Verfügbarkeit und eine gewisse Fehlertoleranz sehr wichtig. Diese wird gewährleistet, durch das Betreiben des Messaging-Systems RabbitMQ in einem Cluster. Bei der Konstruktion des Clusters ist es wichtig, dass Ausfälle einzelner Knoten abgefangen werden können und der Cluster dabei durchgehend verfügbar ist. Da die meisten Systeme, die ein Messaging System verwenden nicht überprüfen, ob die Nachricht den Empfänger erreicht hat, muss ein Datenverlust ausgeschlossen werden. Das Messaging-System muss so konfiguriert werden, dass sowohl im Normal- als auch im Fehlerfall kein Nachrichtenverlust auftritt.\\
\\
Obwohl RabbitMQ Aktiv/Passiv Cluster unterstützt, werden in diesem Projekt ausschließlich Aktiv/Aktiv-Cluster verwendet. Je nach Relevanz der durch RabbitMQ verarbeiteten Daten werden eine unterschiedliche Anzahl an Knoten deployed. Es werden jedoch immer mindestens zwei Knoten deployed um eine Ausfall-Sicherheit zu gewährleisten. Des Weiteren werden mindestens zwei Knoten benötigt, um eine neue Version von RabbitMQ ohne Downtime einzuspielen.\\
\\
Da die \ac{sit} ihre Systeme über zwei Rechenzentren redundant aufbaut, sollte auch RabbitMQ einen Ausfall eines Rechenzentrums abfangen können. Wie \autoref{fig:rabbitmq_rz} darstellt, wird sich auf die Orchestrations-Fähigkeiten von Openshift verlassen, dass niemals alle Pods zur gleichen Zeit im gleichen Rechenzentrum ausgeführt werden.
\begin{figure}
\begin{center}
\includegraphics[scale=0.7]{Bilder/rabbitmq_rz.png}\\
\caption{Beispielhafte Verteilung eines RabbitMQ-Clusters über zwei Rechenzentren (Eigene Darstellung)}
\label{fig:rabbitmq_rz}
\end{center}
\end{figure}

\subsubsection{Plugins}
Aufgrund der großen Anzahl an Plugins ist es unpraktikabel, alle Plugins immer zu installieren, auch wenn diese nicht benötigt werden. Aufgrund dessen muss es ein standardisiertes Vorgehen geben, wie die von dem Kunden gewünschten Plugins in das Messaging System eingebracht werden können. Dies gilt für mitgelieferte Plugins sowie Third-Party-Plugins.

\subsubsection{Sicherheit}
Da es im Vorfeld ungewiss ist, welche Daten von den Kunden versendet werde, ist es nicht Möglich zu sagen, ob diese Daten vertraulich sind. Um sicherzustellen, dass vertrauliche Daten nicht in falsche Hände geraten, muss das Messaging System eine verschlüsselte Verbindung sowohl zu den Clients, als auch zu den Nutzern der Weboberfläche bereitstellen. Des Weiteren soll eine Anbindung an das bereits bestehende LDAP System erstellt werden. Über diese Anbindung soll dann die Authentifizierung erfolgen. Die Autorisierung erfolgt sowohl in Openshift als auch RabbitMQ jeweils intern. Dies sorgt dafür, dass der Verwaltungsaufwand gering bleibt und es verhindert, dass für jedes System neue Benutzer angelegt werden müssen.

\subsubsection{Monitoring}
Die meisten Anwendungen, die ein Messaging System verwenden funktionieren nur angemessen, wenn auch das verwendete Messaging System über eine grundlegende Performance verfügt. Um Performance-Einbrüche schnell zu erkennen oder vorzubeugen ist ein gutes Monitoring des Clusters wichtig. Des Weiteren muss eine Verbindung zu den bestehenden Monitoring- und Alarmsystemen bei der \ac{sit} hergestellt werden, um vor kritischen Situationen zu warnen.

\subsubsection{Deployment}
Im Gegensatz zu den bestehenden Messaging Systemen geht die Tendenz von wenigen aber dafür großen Systemen über zu vielen kleinen Systemen. Aufgrund dessen muss eine große Menge an Messaging-Systemen bereitgestellt werden. Um Zeit zu sparen ist ein einheitlicher Deployment-Prozess wichtig. Dieser Prozess sollte möglichst einfach sein und über eine Benutzer-Oberfläche ausführbar sein. Dies vermindert auch die fachliche Hürde, wodurch auch ein fachlich nicht involvierter Mitarbeiter ein Deployment ausführen kann. Das System soll bis auf Projekt spezifische Einstellungen nicht mehr konfiguriert werden müssen. Die Anbindung an bestehende Systeme soll automatisiert ausgeführt werden. Das Deployment erfolgt durch den für RabbitMQ zuständigen Fachbereich. Dies sorgt für eine einheitliche Installation auf allen Instanzen.

\subsubsection{Backup}
Das Ziel des Konzeptes ist es, RabbitMQ als Integrationswerkzeug zwischen oder in Anwendungen zu verwenden. Es ist nicht angedacht, dass Daten über längere Zeit in RabbitMQ gespeichert werden. Aufgrund dieser Konzeption befinden sich in RabbitMQ nur Daten, die nur kurzzeitig relevant sind. Dies macht eine Backup-Strategie für die Daten überflüssig.

\subsection{Technische Umsetzung}
Dieses Kapitel behandelt die technische Umsetzung der festgeschriebenen Anforderungen (siehe Kapitel \nameref{sec:anforderungen}). Hierzu wird auf die einzelnen Aspekte des Projektes eingegangen. Angefangen mit der grundlegenden Architektur.\\
\\
Aufgrund der Tatsache, dass RabbitMQ in einem Container-Umfeld ausgeführt werden soll, wurde auf die Plattform Openshift zurückgegriffen. Da diese Plattform bereits im Unternehmen \ac{sit} professionell betrieben wird, entfällt dadurch der Aufwand eine eigene Plattform bereitstellen zu müssen. Da Openshift das standard Kubernetes um für das Buissnes Umfeld relevante Themen erweitert, eignet es sich sehr gut für die geplante Nutzung.\\
\\
Um die einzelnen RabbitMQ-Applikationen voneinander zu isolieren, wird für jede Applikation ein neues Openshift Projekt verwendet.

\subsubsection{Aufbau des Openshift-Projekts}
Wie \autoref{fig:namespace} zeigt, werden in einem Namespace drei Applikationen betrieben. Ein Namespace in Kubernetes ist äquivalent zu einem Projekt in Openshift. Zu diesen Applikationen gehört RabbitMQ und zwei Applikationen die dem Monitoring dienen. Dabei handelt es sich um Prometheus und Grafana.
\begin{figure}
\begin{center}
\includegraphics[scale=0.45]{Bilder/namespace.png}\\
\caption{Openshift Projekt (Eigene Darstellung)}
\label{fig:namespace}
\end{center}
\end{figure}

\textbf{RabbitMQ}\\
RabbitMQ wird als StatefulSet in einem Cluster betrieben. Dabei enthält jeder Pod zwei Images. Zum einen RabbitMQ und zum anderen einen Metrics-Exporter, welcher Daten für Prometheus bereitstellt. Die Anzahl der Pods kann während dem Betrieb angepasst werden. Lediglich bei einer dauerhaften Reduktion der Anzahl an Pods, muss der angehängte persistente Storage manuell entfernt werden.\\
\\
\textbf{Prometheus}\\
Genau wie RabbitMQ ist auch Prometheus als Stateful Set installiert. Es ist jedoch nicht vorgesehen, mehrere Instanzen von Prometheus gleichzeitig auszuführen. Dies ist nicht nötig, da dort keine wichtigen Daten gespeichert werden und keine hohe Verfügbarkeit nötig ist. Es wird ein Stateful Set verwendet, da Prometheus zum Speichern der Metriken einen persistenten Speicher benötigt. Der Pod enthält nur das Prometheus Image.\\
\\
\textbf{Grafana}\\
Im Gegensatz zu den beiden anderen Applikationen handelt es sich bei Grafana um ein Deployment. Dies beruht darauf, dass Grafana keinen exklusiven persistenten Storage benötigt. Wie auch bei Prometheus enthält der Pod nur das Grafana Image.

\subsubsection{Networking}
Um die Erreichbarkeit der einzelnen Dienste der Applikationen zu gewährleisten, müssen diese außerhalb des Clusters erreichbar und bekannt gemacht werden. Dies geschieht mithilfe von Services und Routen.\\
\\
Für jeden Port der Applikationen wird ein Service erstellt. Bei RabbitMQ müssen zudem noch Nodeports verwendet werden, da nicht alle Dienste das HTTP Protokoll unterstützen. Für die Verwendung von RabbitMQ als AMQP-Broker werden folgende Ports benötigt:
\begin{itemize}
\item{5672: AMQP}
\item{5671: AMQP SSL}
\item{15672: Webinterface (\ac{http})}
\end{itemize}
Neben dem Webinterface verwenden sowohl Prometheus und Grafana \ac{http}, wodurch man dafür einen normalen Service verwenden kann. Die beiden Services, die einen Node-Port besitzen, sind bereits von externen Clients erreichbar. Für die auf HTTP-basierenden Dienste wird jeweils eine Route angelegt. Um die Cluster-Formation zu gewährleisten wird ein weiterer Service angelegt, der nur diesem Zweck dient.

\subsubsection{Konfiguration}
Um die Funktion der Applikationen zu gewährleisten und um diese auf die Anforderungen des Konzeptes anzupassen, ist eine Konfiguration dieser nötig. Die Konfiguration wird hauptsächlich durch Config-Maps realisiert.\\
\\
\textbf{ConfigMaps}\\
Es gibt für jede benötigte ConfigFile in den Applikationen eine ConfigMap:
\begin{itemize}
\item{rabbitmq-cluster-config}\\
Diese KonfigMap besteht aus zwei Bestandteilen. Zum einen werden hier alle Konfigurationen zum Betrieb von RabbitMQ gespeichert. Des Weiteren alle aktivierten Plugins.
\item{prometheus}\\
In dieser ConfigMap befinden sich Informationen bezüglich des Verhaltens von Prometheus. Des Weiteren auch von welcher Quelle Prometheus die Metriken beziehen soll.
\item{grafana-datasources}\\
In dieser Config-Map befinden sich die Datenquellen von Grafana. In diesem Projekt wird nur auf die Prometheus Instanz referenziert.
\item{grafana-dashboard-provider}\\
Hier wird der Pfad definiert, aus welchem Grafana die Dashboards bezieht.
\end{itemize}
Beim Deployment des Projektes sind die Applikationen bereits initial definiert, sodass diese ohne weitere Konfiguration verwendet werden können. Da diese Konfiguration jedoch sehr allgemein ist, ist es vorgesehen, dass diese nach dem Deployment den Kunden-Anforderungen weiter angepasst werden soll.\\
\\
\textbf{Verschlüsselung}\\
Um alle Kommunikationswege mit dem RabbitMQ-Cluster zu verschlüsseln, ist es nötig das Webinterface per \ac{https} verfügbar zu machen sowie die \ac{amqp} Kommunikation mit \ac{tls} zu verschlüsseln.\\
\\
Die Verschlüsselung des Webinterfaces wird in diesem Fall von Openshift übernommen. Aufgrund der Einstellungen der Openshift-Plattform ist es nicht möglich, eine unverschlüsselte \ac{http} Route zu erstellen. In diesem Fall stellt RabbitMQ eine unverschlüsselte \ac{http} Seite zur Verfügung, die durch eine Openshift-Route verschlüsselt wird und somit \ac{https} verwendet.\\
\\
Da \ac{amqp} nicht auf \ac{http} basiert, kann keine Route verwendet werden. Somit muss RabbitMQ die \ac{tls} Verschlüsselung übernehmen, dazu wird das Zertifikat der Zertifizierungsstelle, das Server-Zertifikat sowie der Server-Key benötigt. Das Zertifikat der Zertifizierungsstelle wird von Kubernetes durch die Verwendung von Service-Accounts automatisch im Container hinterlegt. Dafür muss der Pfad zum Zertifikat in der Konfig-Datei spezifiziert werden. Sowohl Server-Zertifikat als auch Key lassen sich durch die Verwendung einer speziellen Annotation in einem Service als Secret hinterlegen. Diese Secrets können dann als Datei im Dateisystem des Containers hinterlegt werden. Nun müssen diese beiden Dateien in der Konfiguration referenziert werden.\\
\\
Nachdem alle Dateien vorhanden sind, muss der \ac{tls} Betrieb in der Konfiguration aktiviert und der gewünschte Port gewählt werden. In diesem Fall wird der standard Port für \ac{tls} verschlüsselte \ac{amqp} Kommunikation verwendet: 5671.\\
\\
Da sowohl Grafana als auch Prometheus nur \ac{http} verwenden, sind diese wie das Webinterface durch das Verwenden einer Secure-Route verschlüsselt\\
\\
\textbf{Cluster Forming}\\
Das Cluster-Forming von RabbitMQ wird durch das Plugin rabbitmq-peer-discovery-k8s realisiert. Dieses Plugin greift auf die Management API von Kubernetes zu. Dazu müssen dem Service-Account von RabbitMQ view-Rechte in dem Namespace zugewiesen werden. Dies wird über ein Role Binding realisiert. Im Anschluss muss das Plugin aktiviert werden und die Konfiguration, wie in \autoref{fig:cluster-config} zu sehen, der rabbitmq-cluster-config hinzugefügt werden.
\begin{figure}
\begin{center}
\includegraphics[scale=0.9]{Bilder/cluster-conf.png}
\caption{Cluster-Configuration (Eigene Darstellung)}
\label{fig:cluster-config}
\end{center}
\end{figure}
Aufgrund der Tatsache, dass Openshift verwendet wird und kein natives Kubernetes, muss die normale Cluster-Konfiguration um die Zeile \glqq hostname\_suffix\grqq erweitert werden. Des Weiteren müssen alle Knoten über das gleiche Erlang-Cookie verfügen. Dieses wird beim Deployment automatisch auf alle Knoten verteilt. Wenn man nun RabbitMQ mit dieser Konfiguration startet, finden sich die Knoten und skalieren automatisch mit.\\
\\
\subsubsection{Betrieb}
Im laufenden Betrieb müssen Updates eingespielt, Berechtigungen verwaltet und das System überwacht werden. Dieses Kapitel beschäftigt sich mit diesen drei Themen.\\
\\
\textbf{Authentifizierung und Autorisierung}\\
Bei diesem Thema muss zwischen den einzelnen Applikationen differenziert werden.

Bei RabbitMQ erfolgt diee Authentifizierung der Benutzer über das in der \ac{sit} vorhandene \ac{ldap} System. Dies wird über das LDAP Plugin an RabbitMQ angebunden. Jedoch wird es nicht für die Authentifizierung verwendet. Bei jedem neuem Projekt muss ein Projekt-Verantwortlicher bestimmt werden. Dieser erhält administrativen Zugriff auf den RabbitMQ-Cluster und kann weitere Benutzer berechtigen. \\
\\
Da Openshift bereits an das \ac{ldap} System der \ac{sit} angebunden ist, wird es auch hier zur Authentifizierung verwendet. Das Projekt wird von der Abteilung angelegt, die RabbitMQ betreut. Die Rechte zum Betrachten des Projektes werden den Projekt-Beteiligten über die interne Rechteverwaltung von Openshift gewährt.\\
\\
Zum aktuellen Zeitpunkt ist hier weder eine Autorisierung noch eine Authentifizierung realisiert. Zukünftig ist es möglich eine OAuth Authentifizierung über Openshift zu realisieren.\\
\\
\textbf{Monitoring}\\
Das Monitoring wird durch die Applikationen Prometheus und Grafana bewerkstelligt. Damit Prometheus auf die Metriken von RabbitMQ zugreifen kann, muss entweder das Prometheus Plugin verwendet oder ein separater Exporter betrieben werden. In diesem Fall wird ein separater Exporter verwendet, der parallel zu RabbitMQ in dem gleichen Pod ausgeführt wird. Es wurde sich für den separaten Exporter entschieden, da dieser aussagekräftigere Metriken liefert.\\
\\
Damit der Exporter auf die Metriken von RabbitMQ zugreifen kann, benötigt er einen RabbitMQ Benutzer sowie ein Passwort. Dies wird beim Deployment des Templates an den Container des Exporters übergeben. Prometheus sammelt periodisch im Abstand von 15 Sekunden die Metriken von den Exportern ein. Damit Prometheus die einzelnen Exporter selbständig findet, wird ein Mechanismus verwendet der auf die Openshift API zugreift und die verschiedenen Endpoints ermittelt. In den verwendeten Standardeinstellungen speichert Prometheus die Metriken für zwei Wochen. Grafana besitzt eine vordefinierte Schnittstelle für Prometheus und kann über die öffentliche \ac{dns} Adresse von Prometheus direkt auf die Daten zugreifen. Innerhalb von Grafana können einzelne Dashboards von den Kunden erstellt werden, mit Hilfe derer der Status des Clusters überwacht werden kann. Beim Deployment wird ein vordefiniertes Dashboard erstellt.\\
\\
Aktuell besteht noch keine Verbindung zu den Monitoring- und Alerting-Systemen der \ac{sit}. Jedoch besteht zukünftig die Möglichkeit Prometheus Alerts an die zentralen Systeme weiterzuleiten.

\subsubsection{Deployment und Updates}
Das Deployment wird über ein in Openshift hinterlegtes Template ausgeführt. Dieses Template richtet sowohl RabbitMQ als auch Prometheus und Grafana ein (vgl. Anhang). Bei der Erstellung wird der Anwender nach grundlegenden Konfigurationen gefragt. Die initiale Konfiguration wird auch von dem Template übernommen. Nach der erfolgreichen Installation des Templates, kann der Cluster verwendet werden.\\
\\
Die verwendeten Images befinden sich im Docker Hub. Während für den Exporter, Prometheus und Grafana das standard Image verwendet wird, wird das RabbitMQ Image angepasst. Für die einzelnen Versionen von RabbitMQ wird jeweis ein Image mit allen benötigten Plugins erstellt. Dazu wird das original Image von RabbitMQ erweitert, indem die einzelnen Plugins heruntergeladen werden und in dem Plugin-Ordner abgelegt werden. Somit ist es möglich, dass das Image mit den Anforderungen der Kunden wächst.\\
\\
Wie die \autoref{fig:update} darstellt, wird bei der Bereitstellung der benötigten Plugins auf einen zyklischen Prozess zurückgegriffen. Sobald eine neue Image-Version zur Verfügung steht, wird aus diesem Image ein neues internes Image erstellt. Zu Beginn verfügt dieses Image nur über die von RabbitMQ mitgelieferten Plugins. Im nächsten Schritt wird geprüft welche von den Kunden angeforderten Plugins bereits für die neue Version zur Verfügung stehen. Diese werden dann einer Kopie des internen Image hinzugefügt und getestet. Wenn alle neu hinzugefügten Plugins erfolgreich getestet wurden, wird das interne Image ausgetauscht. Wenn weitere Plugins verfügbar sind, wiederholt sich dieser Prozess. Sobald für ein Projekt alle benötigten Plugins in dem internen Image vorhanden sind, kann das Projekt auf die neue Version aktualisiert werden.\\
\begin{figure}
\begin{center}
\includegraphics[scale=0.7]{Bilder/update.png}\\
\caption{Einführen einer neuen RabbitMQ Version (Eigene Darstellung)}
\label{fig:update}
\end{center}
\end{figure}
Um auf eine neue Version von RabbitMQ zu wechseln, gibt es zwei Möglichkeiten. Zum einen ein Update mit Downtime und ein Update ohne Downtime. Welches infrage kommt, hängt davon ab, ob die neue Version von RabbitMQ parallel zu der alten Version in einem Cluster betrieben werden kann. Diese Informationen erhält man von der offiziellen RabbitMQ-Website.\\
\\
Wenn der parallele Betrieb zweier Versionen nicht Möglich ist, müssen alle Cluster-Knoten gestoppt werden. Danach wird das alte Image mit dem der neuen Version in der YAML-Datei des StatefulSets ersetzt. Nun können die einzelnen Knoten wieder gestartet werden, wobei Openshift automatisch das neue Image aus dem Docker Hub herunterlädt.\\
\\
Für den Regelfall, wenn ein paralleler Betrieb möglich ist, kann das Prinzip eines Rolling-Updates angewendet werden. Da dies bereits als Update-Strategie in der \ac{yaml} Datei des Stateful Sets definiert ist, muss lediglich das alte Image durch das neue Image ersetzt werden. Nach Speicherung der \ac{yaml} Datei wird automatisch ein Knoten nach dem anderen heruntergefahren und mit dem neuen Image wieder gestartet. Da während des gesamten Updates jeweils nur ein Pod heruntergefahren ist, bleibt der Cluster durchgehend erreichbar.

\subsection{Ergebnis des Konzeptes}
Dieses Kapitel behandelt das Ergebnis des Konzeptes, also wie die einzelnen Systeme in verschiedenen Szenarien funktioniert haben und welche Probleme aufgetreten sind. Um einen Gesamteindruck zu erhalten, wurde das System wie geplant verwendet, jedoch auch verschiedene Szenarien in Tests simuliert. Hierbei ist zu beachten, dass die ausgeführten Tests nur einen Teil der Szenarien, die im produktiven Umfeld auftreten können, simulieren können. Deshalb kann nicht sichergestellt werden, dass alle Probleme aufgedeckt werden können.

\subsubsection{Deployment}
Das Deployment der Systeme mithilfe eines Templates verlief in allen Fällen ohne Probleme. Es wurden mit verschiedenen Parametern immer alle Container erfolgreich gestartet. Des Weiteren haben sich sowohl die einzelnen RabbitMQ-Instanzen untereinander gefunden, als auch Prometheus und Grafana die benötigten Quellen gefunden. Auch eine anschließende Skalierung der Pods funktionierte ohne Probleme.

\subsubsection{Updates}
\label{sec:updates}
Das einspielen von Updates im Rolling Update Prinzip funktionierte in den meisten Fällen ohne Probleme. In unregelmäßigen Abständen trat der Fall ein, dass ein Knoten sich nicht mehr zu dem Cluster verbinden konnte. Dies konnte durch das Löschen des persistenten Speichers behoben werden. Da die einzelnen Queues über mindestens zwei Knoten gespiegelt sind, kam es dabei zu keinem Datenverlust. Das Problem entstand durch eine fehlerhafte Konfiguration von RabbitMQ. Alle nicht mit dem Cluster verbundenen Knoten wurden im zehn Minuten Intervall aus dem Cluster automatisch entfernt. Wenn zu dieser Zeit zufälligerweise ein Update stattgefunden hat, konnte es passieren, dass ein Knoten aus dem Cluster ausgeschlossen wurde. Dies führt zu einem Konflikt, da der ausgeschlossene Knoten denkt, er gehöre dem Cluster an, der Cluster widerspricht dieser Aussage. Dies konnte durch das Deaktivieren der Cleanup-Funktion behoben werden.

\subsubsection{Ausfall eines Knotens}
Weiter Tests simulieren den Ausfall eines Knotens. Hierzu wurde entweder der Prozess im Container beendet oder der Container gelöscht. Während dessen sendeten mehrere Clients Nachrichten an den Server und überprüften ob alle Nachrichten wieder abgeholt werden konnten. Durch die Spiegelung der Queues kam es auch in diesem Fall zu keinem Nachrichtenverlust. Falls ein Client mit dem ausfallenden Knoten verbunden war, verlor dieser die Verbindung zum Cluster. Dies führt zu einer Exception im Client, da die Verbindung unerwartet beendet wurde. Durch eine Implementierung einer Reconnect-Prozedur, kann der Client wieder Verbindung zum Cluster aufnehmen. Ohne eine dementsprechende Implementierung würde der Client nicht mehr funktionieren. RabbitMQ bietet in der eigenen RabbitMQ Bibliothek für die meisten Sprachen eine automatische Reconnect Prozedur an. Des weiten kann der bereits beschriebene Fall auftreten, dass der Knoten dem Cluster nicht mehr beitreten kann (siehe \fullref{sec:updates}). In jedem Fall wurde der Ausfall des Knotens von Openshift realisiert und automatisch neu gestartet.

\subsubsection{Messaging}
Die Messaging-Funktionalität von RabbitMQ steht im vollem Umfang zu Verfügung. Neben dem aktivierten Standard \ac{amqp}, ist auch die Aktivierung von \ac{mqtt} durch die Aktivierung des Plugins und der Freischaltung der Ports möglich. Als einziger Nachteil hat sich herausgestellt, dass es nicht möglich ist, den \ac{mqtt}- und \ac{amqp}-Port vor dem Deployment festzulegen, da dieser Port dynamisch zugeteilt wird.

\subsubsection{Sonstiges}
Aufgrund eines Problems bei der Herstellung einer gesicherten Verbindung zu dem \ac{ldap} Server, war es in der begrenzten Zeit nicht möglich, eine Anbindung an dieses System zu schaffen. Mit weiterer Bearbeitungszeit ist dies jedoch realisierbar.

